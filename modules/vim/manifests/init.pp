import '*.pp'

class vim($username, $home_parent_dir = '/home') {
  $home = "${home_parent_dir}/${username}"

  include pathogen

  package {
    'vim':
      ensure => installed,
      name   => $operatingsystem ? {
        'CentOS' => 'vim-enhanced',
        default  => 'vim',
      };
  }
  
  Exec { path => '/usr/bin:/bin' }
  File {
    owner   => $username,
    group   => $username,
    require => Package['vim'],
  }

  file {
    ["${home}/.vim", "${home}/.vim/autoload"]:
      ensure => directory;
    "${home}/.vimrc":
      source  => "puppet:///modules/vim/.vimrc";
  }
}
