class vim::pathogen {
  require vim

  $pathogen_path = "${vim::home}/.vim/autoload/pathogen.vim"
  exec {
    'pathogen-install':
      creates => $pathogen_path,
      path    => "/usr/bin",
      command => "curl -so ${pathogen_path} \
        https://raw.github.com/tpope/vim-pathogen/HEAD/autoload/pathogen.vim";
  }

  file {
    "${vim::home}/.vim/bundle":
      ensure => directory;
  }

  define plugin($git_url) {
    exec {
      "${name}-bundle":
        require => File["${vim::home}/.vim/bundle"],
        creates => "${vim::home}/.vim/bundle/${name}",
        cwd     => "${vim::home}/.vim/bundle",
        command => "git clone ${git_url} ${name}";
    }
  }

  plugin {
    'vim-fugitive':
      git_url => 'http://github.com/tpope/vim-fugitive.git';
    'snipmate':
      git_url => 'http://github.com/msanders/snipmate.vim.git';
    'nerdtree':
      git_url => 'http://github.com/scrooloose/nerdtree.git';
    'extradite':
      git_url => 'http://github.com/int3/vim-extradite.git';
    'git-grep':
      git_url => 'http://github.com/tjennings/git-grep-vim.git';
    'puppet-vim':
      git_url => 'http://github.com/ajf/puppet-vim.git';
  }
}
