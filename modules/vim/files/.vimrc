call pathogen#infect()

set bg=dark
set nomodeline
syntax on

"tab settings
set ts=2 sts=2 sw=2 expandtab

filetype plugin indent on

"nerdtree settings
"close vim if only windows left is a NERDtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
"open NERDtree at vim start
autocmd vimenter * NERDTree
